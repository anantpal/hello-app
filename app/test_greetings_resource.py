from unittest import TestCase

import falcon
from falcon import testing

from app.api_controller import api


class TestGreetingModule(TestCase):

    def setUp(self):
        self.client = testing.TestClient(api)

    def test_get_greeting_default_name(self):
        response = self.client.simulate_get('/hello')
        assert response.text == 'Hello!!! John'
        assert response.status == falcon.HTTP_200

    def test_get_greeting_based_on_name(self):
        response = self.client.simulate_get('/hello?name=pal')
        assert response.text == 'Hello!!! pal'
        assert response.status == falcon.HTTP_200
