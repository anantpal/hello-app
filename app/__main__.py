from wsgiref.simple_server import make_server
from app.api_controller import api

if __name__ == '__main__':
    with make_server('', 8080, api) as httpd:
        print('Serving on port 8080...')

        # Serve until process is killed
        httpd.serve_forever()
