import falcon


class GreetingsResource:
    def on_get(self, req, resp):
        """Handles GET requests"""
        resp.status = falcon.HTTP_200  # This is the default status
        resp.body = f'Hello!!! {req.params.get("name", "John")}'


api = falcon.API()
things = GreetingsResource()
api.add_route('/hello', things)
