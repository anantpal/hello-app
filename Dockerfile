FROM python:3.7-alpine

WORKDIR /app

RUN apk update

RUN apk add --virtual=build gcc libffi-dev musl-dev openssl-dev python3-dev && \
    pip3 install --upgrade pip==18.0 setuptools==39.0.1 &&\
    pip install pipenv==2018.7.1

COPY . .

RUN pipenv install

ENTRYPOINT pipenv run python -m app
